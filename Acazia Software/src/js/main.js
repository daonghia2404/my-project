window.onload = () => {
  const customSelect = {
    init: function () {
      this.custom();
    },
    custom: function () {
      var x, i, j, selElmnt, a, b, c;
      /*look for any elements with the class "custom-select":*/
      x = document.getElementsByClassName("custom-select");
      for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        /*for each element, create a new DIV that will act as the selected item:*/
        a = document.createElement("DIV");
        a.setAttribute("class", "select-selected");
        a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(a);
        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 1; j < selElmnt.length; j++) {
          /*for each option in the original select element,
          create a new DIV that will act as an option item:*/
          c = document.createElement("DIV");
          c.innerHTML = selElmnt.options[j].innerHTML;
          c.addEventListener("click", function (e) {
            /*when an item is clicked, update the original select box,
            and the selected item:*/
            var y, i, k, s, h;
            s = this.parentNode.parentNode.getElementsByTagName("select")[0];
            h = this.parentNode.previousSibling;
            for (i = 0; i < s.length; i++) {
              if (s.options[i].innerHTML == this.innerHTML) {
                s.selectedIndex = i;
                h.innerHTML = this.innerHTML;
                y = this.parentNode.getElementsByClassName("same-as-selected");
                for (k = 0; k < y.length; k++) {
                  y[k].removeAttribute("class");
                }
                this.setAttribute("class", "same-as-selected");
                break;
              }
            }
            h.click();
          });
          b.appendChild(c);
        }
        x[i].appendChild(b);
        a.addEventListener("click", function (e) {
          /*when the select box is clicked, close any other select boxes,
          and open/close the current select box:*/
          e.stopPropagation();
          closeAllSelect(this);
          this.nextSibling.classList.toggle("select-hide");
          this.classList.toggle("select-arrow-active");
        });
      }

      function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
          if (elmnt == y[i]) {
            arrNo.push(i)
          } else {
            y[i].classList.remove("select-arrow-active");
          }
        }
        for (i = 0; i < x.length; i++) {
          if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
          }
        }
      }
      /*if the user clicks anywhere outside the select box,
      then close all select boxes:*/
      document.addEventListener("click", closeAllSelect);
    }
  }
  customSelect.init();


  const toggleClass = {
    init: function () {
      this.toggle('.nav__avatar', '.sub__avatar');
      this.toggle('.js--nav-open', '.nav__expand');
      this.toggle('.js--nav-open', 'main');
      this.toggle('.js--nav-open', '.nav__arrow');
      this.toggle('.js--btn', '.block__expand');
      this.toggle('.button__dots', '.button__dots__expand');
    },
    toggle: function (toggleBtn, toggleItem) {
      const btns = document.querySelectorAll(toggleBtn)
      const items = document.querySelectorAll(toggleItem);
      if (toggleBtn === null) return;

      btns.forEach((btn, index) => btn.addEventListener('click', (e) => {
        e.preventDefault();

        btn.classList.toggle('active');
        items[index].classList.toggle('active');
      }))
    }
  }
  toggleClass.init();

  const removeClass = {
    init: function () {
      this.remove();
    },
    remove: function () {
      const btnClose = document.querySelector('.nav__arrow');
      const nav = document.querySelector('.nav__expand');
      const main = document.querySelector('main');
      if (btnClose === null) return;
      btnClose.addEventListener('click', (e) => {
        e.preventDefault();
        nav.classList.remove('active');
        btnClose.classList.remove('active');
        main.classList.remove('active');
      })
    }
  }
  removeClass.init();

  const search = {
    init: function () {
      this.search();
    },
    search: function () {
      const open = document.querySelector('.nav__tools .js--search-btn');
      const close = document.querySelector('.js--search .close');
      const search = document.querySelector('.js--search');
      const overlay = document.querySelector('.overlay-shadow');

      open.addEventListener('click', () => {
        search.classList.add('active');
        overlay.classList.add('active');
      })
      close.addEventListener('click', () => {
        search.classList.remove('active');
        overlay.classList.remove('active');
      })
    }
  }
  search.init();

  const tabs = {
    init: function () {
      this.tab('.blacklog__tabs', '.js--tab-button', '.tabs__expand');
    },
    tab: function (wrap, tabsButton, tabsItem) {
      const self = document.querySelector(wrap);
      if (self === null) return;
      const btns = self.querySelectorAll(tabsButton);
      const items = self.querySelectorAll(tabsItem);

      btns.forEach((btn, index) => btn.addEventListener('click', () => {
        btns[index].classList.toggle('active');
        items[index].classList.toggle('active');
      }));
    }
  }
  tabs.init();

  const dragDrop = {
    init: function () {
      this.effect();
    },
    effect: function () {
      const cols = document.querySelectorAll('table tbody td');
      const items = document.querySelectorAll('table tbody .project');
      let itemDragged = [];


      //Set Attribute and Add event drag All Items
      items.forEach(item => item.setAttribute('draggable', 'true'));

      items.forEach(item => item.addEventListener('dragstart', dragStart));
      items.forEach(item => item.addEventListener('dragend', dragEnd));

      //Drag Item Function
      function dragStart(e) {
        itemDragged.push(e.target);
        this.className += ' dragged';
        setTimeout(() => this.className = 'project hidden', 0);
      }

      function dragEnd() {
        this.className = 'project';
      }

      //Add event drag over All Cols
      cols.forEach(col => col.addEventListener('dragover', dragOver));
      cols.forEach(col => col.addEventListener('dragenter', dragEnter));
      cols.forEach(col => col.addEventListener('dragleave', dragLeave));
      cols.forEach(col => col.addEventListener('drop', dragDrop));

      //Drop Col Funciton
      function dragOver(e) {
        e.preventDefault();
        this.className = 'over';
      }

      function dragEnter(e) {
        this.className = 'over';
      }

      function dragLeave(e) {
        e.preventDefault();
        this.className = '';
      }

      function dragDrop(e) {
        e.preventDefault();
        this.className = '';
        this.append(...itemDragged);
        itemDragged = [];
      }
    }
  }
  dragDrop.init();

  const popup = {
    init: function () {
      this.popup('.js--create', '.js--accept', '.js--cancel', '.popup');
    },
    popup: function (open, accept, cancel, popup) {
      const popupItem = document.querySelector(popup);
      if (popupItem === null) return;

      const openBtn = document.querySelectorAll(open);
      const acceptBtn = popupItem.querySelectorAll(accept);
      const cancelBtn = popupItem.querySelectorAll(cancel);

      if (acceptBtn === null) return;


      openBtn.forEach(item => item.addEventListener('click', (e) => {
        e.preventDefault();
        popupItem.classList.add('active');
      }))

      acceptBtn.forEach(item => item.addEventListener('click', (e) => {
        e.preventDefault();
        popupItem.classList.remove('active');
      }))

      cancelBtn.forEach(item => item.addEventListener('click', (e) => {
        e.preventDefault();
        popupItem.classList.remove('active');
      }))
    }
  }
  popup.init();

  const ckeditor = {
    init: function () {
      this.editor();
    },
    editor: function () {
      const ckeditor = document.querySelector('#editor')
      if (ckeditor === null) return;
      ClassicEditor
        .create(ckeditor)
        .catch(error => {
          console.error(error);
        });
    }
  }
  ckeditor.init();
}